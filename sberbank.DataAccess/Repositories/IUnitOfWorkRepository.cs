namespace sberbnank.DataAccess.Repositories;

public interface IUnitOfWork: IDisposable
{
    IClientRepository Clients { get; }
    IAccountRepository Accounts { get; }
    ITransactionRepository Transactions { get; }
   // Task Save(CancellationToken cancellationToken);
    Task CompleteAsync();
}