using sberbnank.Domain.Entities;

namespace sberbnank.DataAccess.Repositories;

public interface IClientRepository : IBaseRepository<Clients>
{
}