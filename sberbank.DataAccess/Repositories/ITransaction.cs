using sberbnank.Domain.Entities;

namespace sberbnank.DataAccess.Repositories;

public interface ITransactionRepository : IBaseRepository<Transaction>
{
   
}