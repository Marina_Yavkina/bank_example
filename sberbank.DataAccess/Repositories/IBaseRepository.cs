using sberbnank.Domain.Entities;

namespace sberbnank.DataAccess.Repositories;

public interface IBaseRepository<T> where T : Base
{
    void Create(T entity);
    void Update(T entity);
    void Delete(T entity);
    Task<T> Get(Guid id);
    Task<List<T>> GetAll();
}