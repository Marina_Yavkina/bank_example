using sberbnank.Domain.Entities;

namespace sberbnank.DataAccess.Repositories;

public interface IAccountRepository : IBaseRepository<Account>
{
   
}