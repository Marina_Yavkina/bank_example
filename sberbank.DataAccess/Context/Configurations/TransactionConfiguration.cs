using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using sberbnank.Domain.Entities;
using sberbank.DataAccess.Data;

namespace sberbank.DataAccess.Configurations
{
    public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
                 
            builder.Property(a => a.IsDeleted)
                   .HasDefaultValue(false)
                   .IsRequired();

            builder.Property(a => a.Id)
                   .HasColumnType("uuid")
                   .HasDefaultValueSql("uuid_generate_v4()")
                   .IsRequired();

            builder.HasData(SeedData.Transactions);
        }

    }
}
