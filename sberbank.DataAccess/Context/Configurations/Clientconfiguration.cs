using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using sberbnank.Domain.Entities;
using sberbank.DataAccess.Data;

namespace sberbank.DataAccess.Configurations
{
    public class ClientConfiguration : IEntityTypeConfiguration<Clients>
    {
        public void Configure(EntityTypeBuilder<Clients> builder)
        {
            builder.HasMany(a => a.Accounts)
                   .WithOne(t => t.Client)
                   .HasForeignKey(t => t.ClientId)
                   .IsRequired();

            builder.Property(a => a.Id)
                   .HasColumnType("uuid")
                   .HasDefaultValueSql("uuid_generate_v4()")
                   .IsRequired();

            builder.Property(a => a.IsDeleted)
                   .HasDefaultValue(false)
                   .IsRequired();

            builder.HasData(SeedData.Clients);
        }

    }
}
