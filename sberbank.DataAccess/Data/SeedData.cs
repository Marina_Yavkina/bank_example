﻿using sberbnank.DataAccess.Repositories;
using sberbnank.Domain.Entities;
using sberbank.DataAccess.Context;

namespace sberbank.DataAccess.Data
{
    public static class SeedData
    {
        public static IEnumerable<Clients> Clients => new List<Clients>()
        {
            new Clients
            {
                Id = Guid.Parse("f0000000-0000-0000-0000-000000000001"),
                FirstName = "Иван",
                LastName = "Петров"  ,
                Adress =  "21 street",
                Phone = "2334556767878",
                DateCreated = DateTime.Now,
                IsDeleted = false
            },
            new Clients
            {
                 Id = Guid.Parse("f0000000-0000-0000-0000-000000000002"),
                FirstName = "Мария",
                LastName = "Васильева"  ,
                Adress =  "Большой проспект 3",
                Phone = "676787343556",
                DateCreated = DateTime.Now,
                IsDeleted = false
            },
            new Clients
            {
                 Id = Guid.Parse("f0000000-0000-0000-0000-000000000003"),
                FirstName = "Петр",
                LastName = "Сергеев"  ,
                Adress =  "ул. Ленина 5",
                Phone = "56768789000",
                DateCreated = DateTime.Now,
                IsDeleted = false
            },
            new Clients
            {
                 Id = Guid.Parse("f0000000-0000-0000-0000-000000000004"),
                FirstName = "Артем",
                LastName = "Прокофьев"  ,
                Adress =  "ул. Ленина 67",
                Phone = "5668898034",
                DateCreated = DateTime.Now,
                IsDeleted = false
            },
            new Clients
            {
                 Id = Guid.Parse("f0000000-0000-0000-0000-000000000005"),
                FirstName = "Артемида",
                LastName = "Сидорова"  ,
                Adress =  "ул. Зои Космодемьянской 56",
                Phone = "45656768890",
                DateCreated = DateTime.Now,
                IsDeleted = false,
            },
        };
        
        public static IEnumerable<Account> Accounts => new List<Account>()
        {
            new Account
            {
                Id = Guid.Parse("10000000-0000-0000-0006-000000000001"),
                ClientId = Guid.Parse("f0000000-0000-0000-0000-000000000001"),
                Count_number = 121234,
                Money = 3445565567.0,
                DateCreated = DateTime.Now,
                IsDeleted = false
            },
           
            new Account
            {
                Id = Guid.Parse("10000000-0000-0000-0006-000000000002"),
                ClientId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),
                Count_number = 12121218,
                Money = 50000000.0,
                DateCreated = DateTime.Now,
                IsDeleted = false
            },

            new Account
            {
                Id = Guid.Parse("10000000-0000-0000-0006-000000000003"),
                ClientId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),
                Count_number = 121211,
                Money = 6000000.0,
                DateCreated = DateTime.Now,
                IsDeleted = false
            },
            new Account
            {
                Id = Guid.Parse("10000000-0000-0000-0006-000000000004"),
                ClientId = Guid.Parse("f0000000-0000-0000-0000-000000000003"),
                Count_number = 121276,
                Money = 70000000.0,
                DateCreated = DateTime.Now,
                IsDeleted = false
            },
             new Account
            {
                Id = Guid.Parse("10000000-0000-0000-0006-000000000005"),
                ClientId = Guid.Parse("f0000000-0000-0000-0000-000000000004"),
                Count_number = 9876543,
                Money = 8000000.0,
                DateCreated = DateTime.Now,
                IsDeleted = false
            },
        };
        public static IEnumerable<Transaction> Transactions => new List<Transaction>()
        {
            new Transaction
            {
                Id = Guid.Parse("20000000-0000-0000-0006-000000000001"),
                Delta_sum = -121.0,
                DateCreated = DateTime.Now,
                IsDeleted = false,
                AccountId  = Guid.Parse("10000000-0000-0000-0006-000000000002")
            },
           
            new Transaction
            {
                Id = Guid.Parse("20000000-0000-0000-0006-000000000002"),
                Delta_sum = 200.0,
                DateCreated = DateTime.Now,
                IsDeleted = false,
                AccountId  = Guid.Parse("10000000-0000-0000-0006-000000000003")
            },

            new Transaction
            {
                Id = Guid.Parse("20000000-0000-0000-0006-000000000003"),
                Delta_sum = 350.0,
                DateCreated = DateTime.Now,
                IsDeleted = false,
                AccountId  = Guid.Parse("10000000-0000-0000-0006-000000000002")
            },
           new Transaction
            {
                Id = Guid.Parse("20000000-0000-0000-0006-000000000004"),
                Delta_sum = -1000.0,
                DateCreated = DateTime.Now,
                IsDeleted = false,
                AccountId  = Guid.Parse("10000000-0000-0000-0006-000000000003")
            },
              new Transaction
            {
                Id = Guid.Parse("20000000-0000-0000-0006-000000000005"),
                Delta_sum = 2000.0,
                DateCreated = DateTime.Now,
                IsDeleted = false,
                AccountId  = Guid.Parse("10000000-0000-0000-0006-000000000004")
            },
        };
    }
}
