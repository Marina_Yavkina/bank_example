using sberbnank.DataAccess.Repositories;
using sberbnank.Domain.Entities;
using sberbank.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using sberbank.DataAccess.Implemantations;

namespace sberbank.DataAccess.Implemantations;

public class ClientRepository : BaseRepository<Clients>, IClientRepository
{
    public ClientRepository(DataContext context) : base(context)
    {
    }
    
}