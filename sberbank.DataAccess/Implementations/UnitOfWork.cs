using sberbnank.Domain.Entities;
using sberbank.DataAccess.Context;
using sberbnank.DataAccess.Repositories;
using sberbank.DataAccess.Implemantations;

namespace sberbank.DataAccess.Implemantations{

public class UnitOfWork : IUnitOfWork
{
    private readonly DataContext _context;

    public UnitOfWork(DataContext context)
    {
        _context = context;
        Clients = new ClientRepository(_context);
        Accounts = new AccountRepository(_context);
        Transactions = new TransactionRepository(_context);      
    }

    public IClientRepository Clients { get; private set; }

    public IAccountRepository Accounts { get; private set; }

    public ITransactionRepository Transactions { get; private set; }

    public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    public void Dispose()
    {
        _context.Dispose();
    }
}
}