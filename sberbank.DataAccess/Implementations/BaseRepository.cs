using sberbnank.DataAccess.Repositories;
using sberbnank.Domain.Entities;
using sberbank.DataAccess.Context;
using Microsoft.EntityFrameworkCore;

namespace sberbank.DataAccess.Implemantations;

public class BaseRepository<T> : IBaseRepository<T> where T : Base
{
    protected readonly DataContext Context;

    public BaseRepository(DataContext context)
    {
        Context = context;
    }
    
    public void Create(T entity)
    {
        Context.Add(entity);
    }

    public void Update(T entity)
    {
        Context.Update(entity);
    }

    public void Delete(T entity)
    {
        entity.DateCreated = DateTime.UtcNow;
        Context.Update(entity);
    }

    public Task<T> Get(Guid id)
    {
        return Context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
    }

    public Task<List<T>> GetAll()
    {
        return Context.Set<T>().ToListAsync();
    }
}