using sberbnank.DataAccess.Repositories;
using sberbnank.Domain.Entities;
using sberbank.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using sberbank.DataAccess.Implemantations;

namespace sberbank.DataAccess.Implemantations;

public class AccountRepository : BaseRepository<Account>, IAccountRepository
{
    public AccountRepository(DataContext context) : base(context)
    {
    }
    
}