using sberbnank.DataAccess.Repositories;
using sberbnank.Domain.Entities;
using sberbank.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using sberbank.DataAccess.Implemantations;

namespace sberbank.DataAccess.Implemantations;

public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
{
    public TransactionRepository(DataContext context) : base(context)
    {
    }
    
}