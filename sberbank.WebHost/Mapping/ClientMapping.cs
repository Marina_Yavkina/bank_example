using AutoMapper;
using sberbnank.Domain.Entities;
using sberbnank.WebHost.DTO;

namespace sberbnank.WebHost.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    public class ClientMappingsProfile : Profile
    {
        public ClientMappingsProfile()
        {
            CreateMap<ClientDto, Clients>()
             .ForMember(f => f.Accounts, map => map.Ignore());
            CreateMap<Clients,ClientDto>();
        }
    }
}