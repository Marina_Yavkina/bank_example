using sberbnank.DataAccess;
using AutoMapper;
using sberbank.WebHost.Mapper;
using sberbnank.WebHost.Mapping;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.ConfigureContext(builder.Configuration);
builder.Services.AddCors(opt =>
        {
            opt.AddDefaultPolicy(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
        });
var mapperConfig = new MapperConfiguration(
    mc => 
    {
        mc.AddProfile(new ClientMappingsProfile());
    }
);
IMapper mapper = mapperConfig.CreateMapper();
builder.Services.AddSingleton(mapper);
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();


