using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using sberbnank.DataAccess.Repositories;
using sberbnank.Domain.Entities;


namespace sberbank.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionController : ControllerBase
    {
        
        private readonly IUnitOfWork _unitOfWork;

        public TransactionController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Transaction>>> Get()
        {
            var transaction = await _unitOfWork.Transactions.GetAll();
             if (transaction != null)
            {
               
                return Ok(transaction);
            }
        
            return NotFound();
        }

       
        [HttpPost]
        public async Task<ActionResult<Transaction>> CreateLink(Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                 _unitOfWork.Transactions.Create(transaction);
                await _unitOfWork.CompleteAsync();

                return Ok(transaction);
            }

            return BadRequest();
        }
    }
}