using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using sberbnank.DataAccess.Repositories;
using sberbnank.Domain.Entities;


namespace sberbank.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        
        private readonly IUnitOfWork _unitOfWork;

        public AccountController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Account>>> Get()
        {
            var account = await _unitOfWork.Accounts.GetAll();
             if (account != null)
            {
               
                return Ok(account);
            }
        
            return NotFound();
        }

       
        [HttpPost]
        public async Task<ActionResult<Account>> CreateLink(Account account)
        {
            if (ModelState.IsValid)
            {
                 _unitOfWork.Accounts.Create(account);
                await _unitOfWork.CompleteAsync();

                return Ok(account);
            }

            return BadRequest();
        }
    }
}