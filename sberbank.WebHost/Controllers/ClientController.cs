using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using sberbnank.DataAccess.Repositories;
using sberbnank.Domain.Entities;
using sberbnank.WebHost.Mapping;
using AutoMapper;
using sberbnank.WebHost.DTO;


namespace sberbank.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClientController : ControllerBase
    {
        
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public ClientController(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Clients>>> Get()
        {
            var clients = await _unitOfWork.Clients.GetAll();
             if (clients != null)
            {
               
                return Ok(clients);
            }
        
            return NotFound();
        }

       
        [HttpPost]
        public async Task<ActionResult<Clients>> CreateLink(ClientDto client)
        {
            if (ModelState.IsValid)
            {
                var c  = _mapper.Map<Clients>(client);
                 _unitOfWork.Clients.Create(c);
                await _unitOfWork.CompleteAsync();

                return Ok(client);
            }

            return BadRequest();
        }
    }
}