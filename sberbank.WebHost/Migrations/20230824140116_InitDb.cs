﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace sberbank.WebHost.Migrations
{
    /// <inheritdoc />
    public partial class InitDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    FirstName = table.Column<string>(type: "text", nullable: false),
                    LastName = table.Column<string>(type: "text", nullable: false),
                    Adress = table.Column<string>(type: "text", nullable: false),
                    Phone = table.Column<string>(type: "text", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DateDeleted = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    Count_number = table.Column<int>(type: "integer", nullable: false),
                    Money = table.Column<double>(type: "double precision", nullable: false),
                    ClientId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DateDeleted = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Accounts_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    Delta_sum = table.Column<double>(type: "double precision", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DateDeleted = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Adress", "DateCreated", "DateDeleted", "DateUpdated", "FirstName", "LastName", "Phone" },
                values: new object[,]
                {
                    { new Guid("f0000000-0000-0000-0000-000000000001"), "21 street", new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(7340), null, null, "Иван", "Петров", "2334556767878" },
                    { new Guid("f0000000-0000-0000-0000-000000000002"), "Большой проспект 3", new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(7360), null, null, "Мария", "Васильева", "676787343556" },
                    { new Guid("f0000000-0000-0000-0000-000000000003"), "ул. Ленина 5", new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(7370), null, null, "Петр", "Сергеев", "56768789000" },
                    { new Guid("f0000000-0000-0000-0000-000000000004"), "ул. Ленина 67", new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(7370), null, null, "Артем", "Прокофьев", "5668898034" },
                    { new Guid("f0000000-0000-0000-0000-000000000005"), "ул. Зои Космодемьянской 56", new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(7370), null, null, "Артемида", "Сидорова", "45656768890" }
                });

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "Id", "ClientId", "Count_number", "DateCreated", "DateDeleted", "DateUpdated", "Money" },
                values: new object[,]
                {
                    { new Guid("10000000-0000-0000-0006-000000000001"), new Guid("f0000000-0000-0000-0000-000000000001"), 121234, new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8190), null, null, 3445565567.0 },
                    { new Guid("10000000-0000-0000-0006-000000000002"), new Guid("f0000000-0000-0000-0000-000000000002"), 12121218, new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8200), null, null, 50000000.0 },
                    { new Guid("10000000-0000-0000-0006-000000000003"), new Guid("f0000000-0000-0000-0000-000000000002"), 121211, new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8200), null, null, 6000000.0 },
                    { new Guid("10000000-0000-0000-0006-000000000004"), new Guid("f0000000-0000-0000-0000-000000000003"), 121276, new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8200), null, null, 70000000.0 },
                    { new Guid("10000000-0000-0000-0006-000000000005"), new Guid("f0000000-0000-0000-0000-000000000004"), 9876543, new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8210), null, null, 8000000.0 }
                });

            migrationBuilder.InsertData(
                table: "Transactions",
                columns: new[] { "Id", "AccountId", "DateCreated", "DateDeleted", "DateUpdated", "Delta_sum" },
                values: new object[,]
                {
                    { new Guid("20000000-0000-0000-0006-000000000001"), new Guid("10000000-0000-0000-0006-000000000002"), new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8570), null, null, -121.0 },
                    { new Guid("20000000-0000-0000-0006-000000000002"), new Guid("10000000-0000-0000-0006-000000000003"), new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8580), null, null, 200.0 },
                    { new Guid("20000000-0000-0000-0006-000000000003"), new Guid("10000000-0000-0000-0006-000000000002"), new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8580), null, null, 350.0 },
                    { new Guid("20000000-0000-0000-0006-000000000004"), new Guid("10000000-0000-0000-0006-000000000003"), new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8580), null, null, -1000.0 },
                    { new Guid("20000000-0000-0000-0006-000000000005"), new Guid("10000000-0000-0000-0006-000000000004"), new DateTime(2023, 8, 24, 17, 1, 16, 180, DateTimeKind.Local).AddTicks(8580), null, null, 2000.0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_ClientId",
                table: "Accounts",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_AccountId",
                table: "Transactions",
                column: "AccountId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Clients");
        }
    }
}
