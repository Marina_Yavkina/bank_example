namespace sberbnank.WebHost.DTO{
    public class ClientDto
    {
         public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DateCreated { get ; set; }
        public DateTime? DateUpdated { get ; set; }
        public DateTime? DateDeleted { get ; set; }
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public string Adress {get;set;}    
        public string Phone {get;set;}   
    }
}