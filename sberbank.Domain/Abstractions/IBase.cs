namespace sberbnank.Domain.Abstraction
{
    /// <summary>
    /// Интерфейс сущности с идентификатором
    /// </summary>
    /// <typeparam name="TId">Тип идентификатора</typeparam>
    public interface IBase<TId>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        TId Id { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }

        /// <summary>
        /// Возвращает значение true, если элемент был удален
        /// </summary>
        bool IsDeleted { get; set; }
    }
}
