using sberbnank.Domain.Abstraction;

namespace sberbnank.Domain.Entities{
    public class Transaction : Base
    {
        public double Delta_sum {get; set;}
        public Guid AccountId { get; set; }

        /// <summary>
        /// Тренер
        /// </summary>
        public virtual Account Account  { get; set; }

    }
}