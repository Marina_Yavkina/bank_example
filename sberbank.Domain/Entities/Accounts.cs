using sberbnank.Domain.Abstraction;

namespace sberbnank.Domain.Entities{
    public class Account : Base
    {
        public int Count_number{get; set;}   
        public double Money {get; set;}
        public Guid ClientId { get; set; }

        /// <summary>
        /// Тренер
        /// </summary>
        public virtual Clients Client  { get; set; }
        public List<Transaction> Transactions {get;set;}
    }
}