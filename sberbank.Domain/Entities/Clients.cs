using sberbnank.Domain.Abstraction;

namespace sberbnank.Domain.Entities{
    public class Clients : Base
    {
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public string Adress {get;set;}    
        public string Phone {get;set;}     
        public List<Account>? Accounts {get;set;}
    }
}