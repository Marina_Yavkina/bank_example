using sberbnank.Domain.Abstraction;

namespace sberbnank.Domain.Entities{
    public class Base : IBase<Guid>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Удаленная запись доступна только для чтения
        /// </summary>
        public bool IsDeleted { get; set; }
        public DateTime DateCreated { get ; set; }
        public DateTime? DateUpdated { get ; set; }
        public DateTime? DateDeleted { get ; set; }
    }
}
